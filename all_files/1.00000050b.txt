Multiple proliferation-survival signalling pathways are simultaneously active in BRAF V600E mutated thyroid carcinomas.