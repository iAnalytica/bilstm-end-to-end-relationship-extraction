#!/bin/bash

counter=0

for file in *.a2
do
	files=(${files[@]} "$file")
done

type_rel=()

for file in "${files[@]}"
do
#lets try to do only firt row
type_rel=(${type_rel[@]} "$(cat $file | cut -d ' ' -f3)")


((counter++))

done


(IFS=$'\n'; sort <<< "${type_rel[*]}") | uniq -c

echo $counter