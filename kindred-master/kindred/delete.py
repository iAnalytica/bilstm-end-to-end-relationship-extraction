#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 12:43:49 2019

@author: mb618
"""
import pandas as pd
from nltk import word_tokenize
import kindred
import data_helpers
columns = ['id', 'sentence', 'e1', 'e2', 'relation']
df_kirillTrain = pd.DataFrame( columns=columns)

def KindredToDataFrame(Corpus):
    
    #Corpus=kindred.load('standoff',location)
    num=0
    for Corp in Corpus.documents:
        dic_pos=[]
        if Corp.relations != []:
            dic_pos=dic_pos+(Corp.relations[0].entities[0].position)
            dic_pos=dic_pos+(Corp.relations[0].entities[1].position)
            old_sentence=Corp.text
            if dic_pos[1][0]-dic_pos[0][1]==1:
                old_sentence=old_sentence[:dic_pos[0][1]]+' '+old_sentence[:dic_pos[1][0]]
                lst = list(dic_pos[1])
                lst[0]=lst[0]+1
                lst[1]=lst[1]+1
                dic_pos[1] = tuple(lst)
                #dic_pos[1][0]=dic_pos[1][0]+1
                #dic_pos[1][1]=dic_pos[1][1]+1
            new_sentence=old_sentence[:dic_pos[0][0]]+'e11 ' +old_sentence[dic_pos[0][0]:dic_pos[0][1]] +' e12 '+old_sentence[dic_pos[0][1]:dic_pos[1][0]]+ ' e21 '+old_sentence[dic_pos[1][0]:dic_pos[1][1]]+ ' e22'+old_sentence[dic_pos[1][1]:]
            if 'e11' in [word_tokenize(new_sentence)][0] and  'e21' in [word_tokenize(new_sentence)][0]:
                e1=([word_tokenize(new_sentence)][0]).index('e11')
                e2=([word_tokenize(new_sentence)][0]).index('e21')
                relation_type=Corp.relations[0].relationType
        else:
            relation_type='None'
            e1=None
            e2=None
            new_sentence=Corp.text
            
        df_kirillTrain.loc[num] = [num+1,new_sentence,e1,e2,relation_type]
        num=num+1 
    return(df_kirillTrain)


try_data=KindredToDataFrame('/home/mb618/Downloads/civicmine-master/data/training/new_file')
try_data = try_data[try_data.relation != 'None']
#train, validate, test = np.split(try_data.sample(frac=1), [int(.6*len(try_data)), int(.8*len(try_data))])


msk = np.random.rand(len(try_data)) < 0.8

train = try_data[msk]

test = try_data[~msk]

def lstm_format(df): 
    pos1, pos2 = get_relative_position(df, FLAGS.max_sentence_length)

    df['label'] = [utils.class2label[r] for r in df['relation']]

    # Text Data
    x_text = df['sentence'].tolist()
    e1 = df['e1'].tolist()
    e2 = df['e2'].tolist()

    # Label Data
    y = df['label']
    labels_flat = y.values.ravel()
    labels_count = np.unique(labels_flat).shape[0]

    # convert class labels from scalars to one-hot vectors
    # 0  => [1 0 0 0 0 ... 0 0 0 0 0]
    # 1  => [0 1 0 0 0 ... 0 0 0 0 0]
    # ...
    # 18 => [0 0 0 0 0 ... 0 0 0 0 1]
    def dense_to_one_hot(labels_dense, num_classes):
        num_labels = labels_dense.shape[0]
        index_offset = np.arange(num_labels) * num_classes
        labels_one_hot = np.zeros((num_labels, num_classes))
        labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
        return labels_one_hot

    labels = dense_to_one_hot(labels_flat, labels_count)
    labels = labels.astype(np.uint8)
    return x_text, labels, e1, e2, pos1, pos2

train_text, train_y, train_e1, train_e2, train_pos1, train_pos2=lstm_format(train)
test_text, test_y, test_e1, test_e2, test_pos1, test_pos2=lstm_format(test)
test.to_csv("validate_data.csv", sep='\t')

val_data=KindredToDataFrame('/home/mb618/Downloads/civicmine-master/data/testing/new_file')
val_data = val_data[val_data.relation != 'None']

new_text, new_y, new_e1,new_e2,new_pos1,new_pos_2=lstm_format(validate)
last_test_y=[np.where(r==1)[0][0] for r in new_y]



try_data=KindredToDataFrame(kindred.bionlpst.load('2016-BB3-event-train'))
try_data = try_data[try_data.relation != 'None']
#train, validate, test = np.split(try_data.sample(frac=1), [int(.6*len(try_data)), int(.8*len(try_data))])


msk = np.random.rand(len(try_data)) < 0.8

train = try_data[msk]

test = try_data[~msk]