#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 12:43:49 2019

@author: mb618
"""
import pandas as pd
from nltk import word_tokenize
import kindred

import numpy as np
import sys
import os
import time
import tensorflow as tf



from loadFunctions import load
import utils

import warnings
import sklearn.exceptions

from entity_aware_relation_classification_master import logger,configure, data_helpers
from entity_aware_relation_classification_master.data_helpers import get_relative_position
from entity_aware_relation_classification_master.configure import FLAGS
from entity_aware_relation_classification_master import utils_2
from entity_aware_relation_classification_master.logger import Logger
cwd = os.getcwd()

sys.path.insert(1,cwd+'/entity_aware_relation_classification_master')
from model.entity_att_lstm import EntityAttentionLSTM


columns = ['id', 'sentence', 'e1', 'e2', 'relation']
df_kirillTrain = pd.DataFrame( columns=columns)


def KindredToDataFrame(loc='',corp=''):
    
    if loc!='':
        Corpus=kindred.load('standoff',loc)
    if corp!='':
        Coprus=corp
    num=0
    for Corp in Corpus.documents:
        dic_pos=[]
        if Corp.relations != []:
            dic_pos=dic_pos+(Corp.relations[0].entities[0].position)
            dic_pos=dic_pos+(Corp.relations[0].entities[1].position)
            old_sentence=Corp.text
            if dic_pos[1][0]-dic_pos[0][1]==1:
                old_sentence=old_sentence[:dic_pos[0][1]]+' '+old_sentence[:dic_pos[1][0]]
                lst = list(dic_pos[1])
                lst[0]=lst[0]+1
                lst[1]=lst[1]+1
                dic_pos[1] = tuple(lst)
                #dic_pos[1][0]=dic_pos[1][0]+1
                #dic_pos[1][1]=dic_pos[1][1]+1
            new_sentence=old_sentence[:dic_pos[0][0]]+'e11 ' +old_sentence[dic_pos[0][0]:dic_pos[0][1]] +' e12 '+old_sentence[dic_pos[0][1]:dic_pos[1][0]]+ ' e21 '+old_sentence[dic_pos[1][0]:dic_pos[1][1]]+ ' e22'+old_sentence[dic_pos[1][1]:]
            if 'e11' in [word_tokenize(new_sentence)][0] and  'e21' in [word_tokenize(new_sentence)][0]:
                e1=([word_tokenize(new_sentence)][0]).index('e11')
                e2=([word_tokenize(new_sentence)][0]).index('e21')
                relation_type=Corp.relations[0].relationType
        else:
            relation_type='None'
            e1=None
            e2=None
            new_sentence=Corp.text
            
        df_kirillTrain.loc[num] = [num+1,new_sentence,e1,e2,relation_type]
        num=num+1 
    return(df_kirillTrain)


try_data=KindredToDataFrame(loc='/home/mb618/Downloads/civicmine-master/data/training/new_file')
try_data = try_data[try_data.relation != 'None']
#train, validate, test = np.split(try_data.sample(frac=1), [int(.6*len(try_data)), int(.8*len(try_data))])


msk = np.random.rand(len(try_data)) < 0.8

train = try_data[msk]

test = try_data[~msk]

def lstm_format(df): 
    pos1, pos2 = get_relative_position(df, FLAGS.max_sentence_length)

    df['label'] = [utils_2.class2label[r] for r in df['relation']]

    # Text Data
    x_text = df['sentence'].tolist()
    e1 = df['e1'].tolist()
    e2 = df['e2'].tolist()

    # Label Data
    y = df['label']
    labels_flat = y.values.ravel()
    labels_count = np.unique(labels_flat).shape[0]

    # convert class labels from scalars to one-hot vectors
    # 0  => [1 0 0 0 0 ... 0 0 0 0 0]
    # 1  => [0 1 0 0 0 ... 0 0 0 0 0]
    # ...
    # 18 => [0 0 0 0 0 ... 0 0 0 0 1]
    def dense_to_one_hot(labels_dense, num_classes):
        num_labels = labels_dense.shape[0]
        index_offset = np.arange(num_labels) * num_classes
        labels_one_hot = np.zeros((num_labels, num_classes))
        labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
        return labels_one_hot

    labels = dense_to_one_hot(labels_flat, labels_count)
    labels = labels.astype(np.uint8)
    return x_text, labels, e1, e2, pos1, pos2

train_text, train_y, train_e1, train_e2, train_pos1, train_pos2=lstm_format(train)
test_text, test_y, test_e1, test_e2, test_pos1, test_pos2=lstm_format(test)
test.to_csv("validate_data.csv", sep='\t')





#try_data=KindredToDataFrame(kindred.bionlpst.load('2016-BB3-event-train'))
#try_data = try_data[try_data.relation != 'None']
#train, validate, test = np.split(try_data.sample(frac=1), [int(.6*len(try_data)), int(.8*len(try_data))])


msk = np.random.rand(len(try_data)) < 0.8

train = try_data[msk]

test = try_data[~msk]

def train_lstm(train_text, train_y, train_e1, train_e2, train_pos1, train_pos2,test_text, test_y, test_e1, test_e2, test_pos1, test_pos2):
 

    # Build vocabulary
    # Example: x_text[3] = "A misty <e1>ridge</e1> uprises from the <e2>surge</e2>."
    # ['a misty ridge uprises from the surge <UNK> <UNK> ... <UNK>']
    # =>
    # [27 39 40 41 42  1 43  0  0 ... 0]
    # dimension = MAX_SENTENCE_LENGTH
    vocab_processor = tf.contrib.learn.preprocessing.VocabularyProcessor(FLAGS.max_sentence_length)
    vocab_processor.fit(list(train_text) + list(test_text))
    train_x = np.array(list(vocab_processor.transform(train_text)))
    test_x = np.array(list(vocab_processor.transform(test_text)))
    train_text = np.array(train_text)
    test_text = np.array(test_text)
    print("\nText Vocabulary Size: {:d}".format(len(vocab_processor.vocabulary_)))
    print("train_x = {0}".format(train_x.shape))
    print("train_y = {0}".format(train_y.shape))
    print("test_x = {0}".format(test_x.shape))
    print("test_y = {0}".format(test_y.shape))

    # Example: pos1[3] = [-2 -1  0  1  2   3   4 999 999 999 ... 999]
    # [95 96 97 98 99 100 101 999 999 999 ... 999]
    # =>
    # [11 12 13 14 15  16  21  17  17  17 ...  17]
    # dimension = MAX_SENTENCE_LENGTH
    pos_vocab_processor = tf.contrib.learn.preprocessing.VocabularyProcessor(FLAGS.max_sentence_length)
    pos_vocab_processor.fit(train_pos1 + train_pos2 + test_pos1 + test_pos2)
    train_p1 = np.array(list(pos_vocab_processor.transform(train_pos1)))
    train_p2 = np.array(list(pos_vocab_processor.transform(train_pos2)))
    test_p1 = np.array(list(pos_vocab_processor.transform(test_pos1)))
    test_p2 = np.array(list(pos_vocab_processor.transform(test_pos2)))
    print("\nPosition Vocabulary Size: {:d}".format(len(pos_vocab_processor.vocabulary_)))
    print("train_p1 = {0}".format(train_p1.shape))
    print("test_p1 = {0}".format(test_p1.shape))
    print("")


    with tf.Graph().as_default():
        session_conf = tf.ConfigProto(
            allow_soft_placement=FLAGS.allow_soft_placement,
            log_device_placement=FLAGS.log_device_placement)
        session_conf.gpu_options.allow_growth = FLAGS.gpu_allow_growth
        sess = tf.Session(config=session_conf)
        with sess.as_default():
            model = EntityAttentionLSTM(
                sequence_length=train_x.shape[1],
                num_classes=train_y.shape[1],
                vocab_size=len(vocab_processor.vocabulary_),
                embedding_size=FLAGS.embedding_size,
                pos_vocab_size=len(pos_vocab_processor.vocabulary_),
                pos_embedding_size=FLAGS.pos_embedding_size,
                hidden_size=FLAGS.hidden_size,
                num_heads=FLAGS.num_heads,
                attention_size=FLAGS.attention_size,
                use_elmo=(FLAGS.embeddings == 'elmo'),
                l2_reg_lambda=FLAGS.l2_reg_lambda)

            # Define Training procedure
            global_step = tf.Variable(0, name="global_step", trainable=False)
            optimizer = tf.train.AdadeltaOptimizer(FLAGS.learning_rate, FLAGS.decay_rate, 1e-6)
            gvs = optimizer.compute_gradients(model.loss)
            capped_gvs = [(tf.clip_by_value(grad, -1.0, 1.0), var) for grad, var in gvs]
            train_op = optimizer.apply_gradients(capped_gvs, global_step=global_step)

            # Output directory for models and summaries
            timestamp = str(int(time.time()))
            out_dir = os.path.abspath(os.path.join(os.path.curdir, "runs", timestamp))
            print("\nWriting to {}\n".format(out_dir))

            # Logger
            logger = Logger(out_dir)

            # Summaries for loss and accuracy
            loss_summary = tf.summary.scalar("loss", model.loss)
            acc_summary = tf.summary.scalar("accuracy", model.accuracy)

            # Train Summaries
            train_summary_op = tf.summary.merge([loss_summary, acc_summary])
            train_summary_dir = os.path.join(out_dir, "summaries", "train")
            train_summary_writer = tf.summary.FileWriter(train_summary_dir, sess.graph)

            # Checkpoint directory. Tensorflow assumes this directory already exists so we need to create it
            checkpoint_dir = os.path.abspath(os.path.join(out_dir, "checkpoints"))
            checkpoint_prefix = os.path.join(checkpoint_dir, "model")
            if not os.path.exists(checkpoint_dir):
                os.makedirs(checkpoint_dir)
            saver = tf.train.Saver(tf.global_variables(), max_to_keep=FLAGS.num_checkpoints)

            # Write vocabulary
            vocab_processor.save(os.path.join(out_dir, "vocab"))
            pos_vocab_processor.save(os.path.join(out_dir, "pos_vocab"))

            # Initialize all variables
            sess.run(tf.global_variables_initializer())

            if FLAGS.embeddings == "word2vec":
                pretrain_W = utils.load_word2vec('resource/GoogleNews-vectors-negative300.bin', FLAGS.embedding_size, vocab_processor)
                sess.run(model.W_text.assign(pretrain_W))
                print("Success to load pre-trained word2vec model!\n")
            elif FLAGS.embeddings == "glove100":
                pretrain_W = utils.load_glove('resource/glove.6B.100d.txt', FLAGS.embedding_size, vocab_processor)
                sess.run(model.W_text.assign(pretrain_W))
                print("Success to load pre-trained glove100 model!\n")
            elif FLAGS.embeddings == "glove300":
                pretrain_W = utils.load_glove('resource/glove.840B.300d.txt', FLAGS.embedding_size, vocab_processor)
                sess.run(model.W_text.assign(pretrain_W))
                print("Success to load pre-trained glove300 model!\n")

            # Generate batches
            train_batches = data_helpers.batch_iter(list(zip(train_x, train_y, train_text,
                                                             train_e1, train_e2, train_p1, train_p2)),
                                                    FLAGS.batch_size, FLAGS.num_epochs)
            # Training loop. For each batch...
            best_f1 = 0.0  # For save checkpoint(model)
            for train_batch in train_batches:
                train_bx, train_by, train_btxt, train_be1, train_be2, train_bp1, train_bp2 = zip(*train_batch)
                feed_dict = {
                    model.input_x: train_bx,
                    model.input_y: train_by,
                    model.input_text: train_btxt,
                    model.input_e1: train_be1,
                    model.input_e2: train_be2,
                    model.input_p1: train_bp1,
                    model.input_p2: train_bp2,
                    model.emb_dropout_keep_prob: FLAGS.emb_dropout_keep_prob,
                    model.rnn_dropout_keep_prob: FLAGS.rnn_dropout_keep_prob,
                    model.dropout_keep_prob: FLAGS.dropout_keep_prob
                }
                _, step, summaries, loss, accuracy = sess.run(
                    [train_op, global_step, train_summary_op, model.loss, model.accuracy], feed_dict)
                train_summary_writer.add_summary(summaries, step)

                # Training log display
                if step % FLAGS.display_every == 0:
                    logger.logging_train(step, loss, accuracy)

                # Evaluation
                if step % FLAGS.evaluate_every == 0:
                    print("\nEvaluation:")
                    # Generate batches
                    test_batches = data_helpers.batch_iter(list(zip(test_x, test_y, test_text,
                                                                    test_e1, test_e2, test_p1, test_p2)),
                                                           FLAGS.batch_size, 1, shuffle=False)
                    # Training loop. For each batch...
                    losses = 0.0
                    accuracy = 0.0
                    predictions = []
                    iter_cnt = 0
                    for test_batch in test_batches:
                        test_bx, test_by, test_btxt, test_be1, test_be2, test_bp1, test_bp2 = zip(*test_batch)
                        feed_dict = {
                            model.input_x: test_bx,
                            model.input_y: test_by,
                            model.input_text: test_btxt,
                            model.input_e1: test_be1,
                            model.input_e2: test_be2,
                            model.input_p1: test_bp1,
                            model.input_p2: test_bp2,
                            model.emb_dropout_keep_prob: 1.0,
                            model.rnn_dropout_keep_prob:1.0,
                            model.dropout_keep_prob: 1.0
                        }
                        loss, acc, pred = sess.run(
                            [model.loss, model.accuracy, model.predictions], feed_dict)
                        losses += loss
                        accuracy += acc
                        predictions += pred.tolist()
                        iter_cnt += 1
                    losses /= iter_cnt
                    accuracy /= iter_cnt
                    predictions = np.array(predictions, dtype='int')
                    print (step, loss, accuracy, predictions)

                    logger.logging_eval(step, loss, accuracy, predictions)

                    # Model checkpoint
                    if best_f1 < logger.best_f1:
                        best_f1 = logger.best_f1
                        path = saver.save(sess, checkpoint_prefix+"-{:.3g}".format(best_f1), global_step=step)
                        print(checkpoint_prefix)
                        print("Saved model checkpoint to {}\n".format(path))
                        final_path = checkpoint_prefix[:-18]
    return(final_path)
    
final_path=train_lstm(train_text, train_y, train_e1, train_e2, train_pos1, train_pos2,test_text, test_y, test_e1, test_e2, test_pos1, test_pos2)

val_data=KindredToDataFrame('/home/mb618/Downloads/civicmine-master/data/testing/new_file')
val_data = val_data[val_data.relation != 'None']

new_text, new_y, new_e1,new_e2,new_pos1,new_pos_2=lstm_format(val_data)
last_test_y=[np.where(r==1)[0][0] for r in new_y]

real_values=[np.where(r==1)[0][0] for r in test_y]
checkpoint_dir=final_path+"/checkpoints"
checkpoint_file=tf.train.latest_checkpoint(final_path+"/checkpoints")
print(checkpoint_file)


vocab_processor = tf.contrib.learn.preprocessing.VocabularyProcessor.restore(final_path+"/vocab")

# Map data into position
pos_vocab_processor = tf.contrib.learn.preprocessing.VocabularyProcessor.restore(final_path+"/pos_vocab")

test_x = np.array(list(vocab_processor.transform(test_text)))
test_text = np.array(test_text)
print("\nText Vocabulary Size: {:d}".format(len(vocab_processor.vocabulary_)))
print("test_x = {0}".format(test_x.shape))
print("test_y = {0}".format(test_y.shape))

test_p1 = np.array(list(pos_vocab_processor.transform(test_pos1)))
test_p2 = np.array(list(pos_vocab_processor.transform(test_pos2)))
print("\nPosition Vocabulary Size: {:d}".format(len(pos_vocab_processor.vocabulary_)))
print("test_p1 = {0}".format(test_p1.shape))
print("")

graph = tf.Graph()
with graph.as_default():
    session_conf = tf.ConfigProto(
        allow_soft_placement=FLAGS.allow_soft_placement,
        log_device_placement=FLAGS.log_device_placement)
    session_conf.gpu_options.allow_growth = FLAGS.gpu_allow_growth
    sess = tf.Session(config=session_conf)
    with sess.as_default():
        # Load the saved meta graph and restore variables
        saver = tf.train.import_meta_graph("{}.meta".format(checkpoint_file))
        saver.restore(sess, checkpoint_file)

        input_x = graph.get_operation_by_name("input_x").outputs[0]
        input_y = graph.get_operation_by_name("input_y").outputs[0]
        input_text = graph.get_operation_by_name("input_text").outputs[0]
        input_e1 = graph.get_operation_by_name("input_e1").outputs[0]
        input_e2 = graph.get_operation_by_name("input_e2").outputs[0]
        input_p1 = graph.get_operation_by_name("input_p1").outputs[0]
        input_p2 = graph.get_operation_by_name("input_p2").outputs[0]
        emb_dropout_keep_prob = graph.get_operation_by_name("emb_dropout_keep_prob").outputs[0]
        rnn_dropout_keep_prob = graph.get_operation_by_name("rnn_dropout_keep_prob").outputs[0]
        dropout_keep_prob = graph.get_operation_by_name("dropout_keep_prob").outputs[0]
        self_alphas_op = graph.get_operation_by_name("self-attention/multihead_attention/Softmax").outputs[0]
        alphas_op = graph.get_operation_by_name("attention/alphas").outputs[0]
        acc_op = graph.get_operation_by_name("accuracy/accuracy").outputs[0]
        e2_alphas_op = graph.get_operation_by_name("attention/e2_alphas").outputs[0]
        e1_alphas_op = graph.get_operation_by_name("attention/e1_alphas").outputs[0]
        latent_type_op = graph.get_operation_by_name("attention/latent_type").outputs[0]

        print("\nEvaluation:")
        # Generate batches
        test_batches = data_helpers.batch_iter(list(zip(test_x, test_y, test_text,
                                                        test_e1, test_e2, test_p1, test_p2)),
                                               FLAGS.batch_size, 1, shuffle=False)
        # Training loop. For each batch...
        accuracy = 0.0
        iter_cnt = 0
        predictions=[]
        with open("visualization.html", "w") as html_file:
            for test_batch in test_batches:
                test_bx, test_by, test_btxt, test_be1, test_be2, test_bp1, test_bp2 = zip(*test_batch)
                feed_dict = {
                    input_x: test_bx,
                    input_y: test_by,
                    input_text: test_btxt,
                    input_e1: test_be1,
                    input_e2: test_be2,
                    input_p1: test_bp1,
                    input_p2: test_bp2,
                    emb_dropout_keep_prob: 1.0,
                    rnn_dropout_keep_prob: 1.0,
                    dropout_keep_prob: 1.0
                }
                self_alphas, alphas, acc, e1_alphas, e2_alphas, latent_type = sess.run(
                    [self_alphas_op, alphas_op, acc_op, e1_alphas_op, e2_alphas_op, latent_type_op], feed_dict)
                accuracy += acc
                iter_cnt += 1
                #predictions=predictions+[np.where(r==1)[0][0] for r in test_by]
                #print(latent_type[0])
                for text, alphas_values in zip(test_btxt, alphas):
                    for word, alpha in zip(text.split(), alphas_values / alphas_values.max()):
                        html_file.write(
                            '<font style="background: rgba(255, 255, 0, %f)">%s</font>\n' % (alpha, word))
                    html_file.write('<br>')
        accuracy /= iter_cnt
        #f1_score=sklearn.metrics.f1_score(real_values,predictions,average='weighted')
        #print(predictions)
        #print(f1_score)
        print(accuracy)