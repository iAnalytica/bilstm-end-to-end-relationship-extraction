#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 17 16:27:51 2019

@author: mb618
"""



#calculate f1-score logistic regression different threholds-the best threshold is .87
grid=np.arange(0,1,0.1)

all_f1scores=[]
train=kindred.load('standoff','/home/mb618/Downloads/all_files')
for i in grid:
    trainCorpus,devCorpus=train.split(.80)
    predictionCorpus = devCorpus.clone()
    predictionCorpus.removeRelations()
    classifier_2 = kindred.RelationClassifier(classifierType='LogisticRegression',threshold=i)
    classifier_2.train(trainCorpus)
    classifier_2.predict(predictionCorpus)
    f1score = kindred.evaluate(devCorpus, predictionCorpus, metric='f1score')
    all_f1scores=all_f1scores+[f1score]
    print(f1score)

plt.plot(grid, all_f1scores[:], label = "line 1")
plt.xlabel('Threshold')
plt.ylabel('F1-score')
plt.savefig('logitic_regression_different_thresholds')

#calculate the power_rule

all_f1scores=[]
grid_split=np.arange(.10,.8,.05)
train=kindred.load('standoff','/home/mb618/Downloads/all_files')

for i in grid_split:
    trainCorpus,devCorpus=train.split(i)
    predictionCorpus = devCorpus.clone()
    predictionCorpus.removeRelations()
    classifier_2 = kindred.RelationClassifier(classifierType='LogisticRegression',threshold=.87)
    classifier_2.train(trainCorpus)
    classifier_2.predict(predictionCorpus)
    f1score = kindred.evaluate(devCorpus, predictionCorpus, metric='f1score')
    all_f1scores=all_f1scores+[f1score]
    print(f1score)

total= [ 73.6, 110.4, 147.2, 184. , 220.8, 257.6, 294.4, 331.2, 368. ,404.8, 441.6, 478.4, 515.2, 552. ]
np.dot(total,grid_split)
    


plt.plot(total, all_f1scores=[.24,.27,.326,.337,.35,.35,.38,.39,.39,.43,.39,.38,.44,.46],label="line 1")
plt.xlabel('number_of_samples')
plt.ylabel('F1-score')
plt.savefig('power_law')



c_list=list(range(1,10))
f=0
max_c=0
train=kindred.load('standoff','/home/mb618/Downloads/all_files')
trainCoupus,inter=train.split(0.6)
devCorpus,testCorpus =inter.split(0.5)

for i in c_list:
    train=kindred.load('standoff','/home/mb618/Downloads/all_files')
    predictionCorpus = devCorpus.clone()
    predictionCorpus.removeRelations()
    classifier_2 = kindred.RelationClassifier(c_value=i)
    classifier_2.train(trainCorpus)
    classifier_2.predict(predictionCorpus)
    f1score = kindred.evaluate(devCorpus, predictionCorpus, metric='f1score')
    print(f1score)
    if f1score>f:
        f=f1score
        max_c=i
final=0
percentage_list=[.20,.25,.30,.35,.40,45,.50,.55,.60,.65.70,.75,.80]
score_for_per=[]
for per in percentage_list:
    for i in range(20):
        trainCoupus,devCorpus=train.split(per)
        
        predictionCorpus = devCorpus.clone()
        predictionCorpus.removeRelations()
        classifier_2 = kindred.RelationClassifier(c_value=3)
        classifier_2.train(trainCorpus)
        classifier_2.predict(predictionCorpus)
        f1score = kindred.evaluate(devCorpus, predictionCorpus, metric='f1score')
        final=final+f1score
        print(f1score)
    final=final/20
    score_for_per.append(final)
    print(score_for_per)


f=0
max_c=0
c_list=list(range(1,10))
trainCorpus = kindred.bionlpst.load('2016-BB3-event-train')
devCorpus = kindred.bionlpst.load('2016-BB3-event-dev')
for i in c_list:
    predictionCorpus = devCorpus.clone()
    predictionCorpus.removeRelations()
    classifier_2 = kindred.RelationClassifier(c_value=i)
    classifier_2.train(trainCorpus)
    classifier_2.predict(predictionCorpus)
    f1score = kindred.evaluate(devCorpus, predictionCorpus, metric='f1score')
    print(f1score)
    if f1score>f:
        f=f1score
        max_c=i

for i in range(10):
    trainCoupus,devCorpus=kindred.bionlpst.load('2016-BB3-event-train').split(0.8)
    
    predictionCorpus = devCorpus.clone()
    predictionCorpus.removeRelations()
    classifier_2 = kindred.RelationClassifier(c_value=2)
    classifier_2.train(trainCorpus)
    classifier_2.predict(predictionCorpus)
    f1score = kindred.evaluate(devCorpus, predictionCorpus, metric='f1score')
    final=final+f1score
    print(f1score)
final=final/10