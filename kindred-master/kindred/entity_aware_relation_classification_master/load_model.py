#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 14:46:53 2019

@author: mb618
"""

'''with tf.Session() as sess:
    saver = tf.train.import_meta_graph('/home/mb618/Downloads/entity-aware-relation-classification-master/runs/1562246830/checkpoints/model-0.844-600.meta')
    saver.restore(sess, "/home/mb618/Downloads/entity-aware-relation-classification-master/runs/1562246830/checkpoints/checkpoint")
    
Python'''

with tf.Session() as sess:    
     saver = tf.train.import_meta_graph('/home/mb618/Downloads/entity-aware-relation-classification-master/runs/1562246830/checkpoints/model-0.844-600.meta')
     saver.restore(sess, '/home/mb618/Downloads/entity-aware-relation-classification-master/runs/1562246830/checkpoints/checkpoint')
     print(sess.run('w1:0'))
##Model has been restored. Above statement will print the saved value of w1.
with tf.Session() as sess:
   # Restore variables from disk.
   saver.restore(sess, "/tmp/model.ckpt")
   print("Model restored.")
   # Check the values of the variables
   print("v1 : %s" % v1.eval())
   print("v2 : %s" % v2.eval())